import deck.Deck;
import deck.Hand;

public class Game {
    public static void main(String [] args) {
        Deck deck = new Deck();
        System.out.println(deck);
        deck.shuffle();
        Hand newHand = new Hand( deck.deal(5) );
        System.out.println(newHand);
        newHand.sort();
        System.out.println(newHand);

    }
}
