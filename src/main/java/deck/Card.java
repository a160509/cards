package deck;

import java.util.Comparator;

public abstract class Card {
    protected int _value;

    protected int get_value() { return _value; }

    protected static Comparator<Card> compCardVals = new Comparator<Card>() {
        public int compare(Card c1, Card c2) {
            return c2.get_value() - c1.get_value();
        }
    };
}
