package deck;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Deck {
    private List<PokerCard> deck = new ArrayList<PokerCard>();
    private List<PokerCard> cardsInPlay = new ArrayList<PokerCard>();

    public Deck() {
        for (Face face: Face.values()) {
            for (Suit suit: Suit.values()) {
                this.deck.add( new PokerCard(face, suit) );
            }
        }
    }

    public void shuffle() {
        List<PokerCard> newDeck = new ArrayList<PokerCard>(this.deck.size());
        Random rand = new Random();
        int intervals = deck.size();
        int position;

        for (int i = 0; i < intervals; i++) {
            position = rand.nextInt(deck.size());
            newDeck.add(this.deck.get(position));
            deck.remove(position);
        }

        this.deck = newDeck;
    }

    public PokerCard pop() {
        PokerCard poppedCard;
        int lastCardPosition = this.deck.size() - 1;
        poppedCard = this.deck.get(lastCardPosition);
        this.deck.remove(lastCardPosition);
        cardsInPlay.add(poppedCard);
        return poppedCard;
    }

    public List<PokerCard> deal(int cardsToDraw) {
        List<PokerCard> cardSet = new ArrayList<PokerCard>(cardsToDraw);
        for (int i = 0; i < cardsToDraw; i++) {
            cardSet.add(this.pop());
        }
        return cardSet;
    }

    public void recombine() {
        deck.addAll(cardsInPlay);
        cardsInPlay.clear();
    }

    @Override
    public String toString() {
        return "Remaining Cards: " + deck.size() + "  In play: " + cardsInPlay.size();
    }
}