package deck;

public enum Face {
    TWO(20),
    THREE(30),
    FOUR(40),
    FIVE(50),
    SIX(60),
    SEVEN(70),
    EIGHT(80),
    NINE(90),
    TEN(100),
    JACK(110),
    QUEEN(120),
    KING(130),
    ACE(140);

    private int value;

    public int getValue() {
        return value;
    }

    Face(int value) {
        this.value = value;
    }
}
