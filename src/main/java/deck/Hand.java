package deck;

import java.util.ArrayList;
import java.util.List;

public class Hand {

    private List<PokerCard> _cards = new ArrayList<PokerCard>();

    public Hand(List<PokerCard> hand) {
        _cards.addAll(hand);
    }

    public List<PokerCard> get_cards() {
        return _cards;
    }

    @Override
    public String toString() {
        return get_cards().toString();
    }

    public void sort() {
        _cards.sort(PokerCard.compCardVals);
    }

}
