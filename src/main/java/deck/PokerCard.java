package deck;

public class PokerCard extends Card {
    private Suit _suit;
    private Face _face;

    public PokerCard(Face _face, Suit _suit) {
        this._face = _face;
        this._suit = _suit;
        this._value = _face.getValue() + _suit.getValue();
    }

    public Suit get_suit() {
        return _suit;
    }
    public Face get_face() {
        return _face;
    }

    @Override
    public String toString() {
        return this.get_face().name().concat(" of ").concat(this.get_suit().name());
    }

}
