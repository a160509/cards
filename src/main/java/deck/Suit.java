package deck;

public enum Suit {
    SPADES(1),
    HEARTS(2),
    CLUBS(3),
    DIAMONDS(4);

    private int value;

    public int getValue() {
        return value;
    }

    Suit(int value) {
        this.value = value;
    }
}
